#!/usr/bin/env python
import connexion

app = connexion.FlaskApp(__name__, specification_dir='./flask_microservice/swagger')

app.add_api('swagger.yaml')
app.run(port=9099,server='tornado')
