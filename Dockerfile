FROM python:3.6

ADD . /opt/simple-api
WORKDIR /opt/simple-api
VOLUME /opt/simple-api

RUN pip install -r requirements.txt

EXPOSE 9099

ENTRYPOINT python run.py
