import os
import sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../..")))

from flask_microservice import app

app.testing = True
test_app = app.app.test_client()
